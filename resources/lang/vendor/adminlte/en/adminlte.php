<?php

return [

    'full_name'                   => 'Nombre Completo',
    'email'                       => 'Email',
    'password'                    => 'Contraseña',
    'retype_password'             => 'Vuelva a escribir la contraseña',
    'remember_me'                 => 'Recuérdame',
    'register'                    => 'Registrarse',
    'register_a_new_membership'   => 'Registrar una nueva membresía',
    'i_forgot_my_password'        => 'Olvidé mi contraseña',
    'i_already_have_a_membership' => 'Ya tengo una membresía',
    'sign_in'                     => 'Registrarse',
    'log_out'                     => 'Cerrar Sesión',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Sign in to start your session',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Send Password Reset Link',
    'verify_message'              => 'Your account needs a verification',
    'verify_email_sent'           => 'A fresh verification link has been sent to your email address.',
    'verify_check_your_email'     => 'Before proceeding, please check your email for a verification link.',
    'verify_if_not_recieved'      => 'If you did not receive the email',
    'verify_request_another'      => 'click here to request another',
    'confirm_password_message'    => 'Please, confirm your password to continue.',
];
