@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
    @error('Escuela')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error!</strong> Por favor llena los campos en rojo correctamente
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @php
        $Registro = 0;
    @endphp
    @enderror
    @if($Registro == 1)
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Exito!</strong> Se agrego el Tipo de media Correctamente
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-header">
                        Añadir Escuelas
                    </div>
                    <div class="card-body">
                        <form action="{{url('/NewUniversidad')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="Escuela">Nombre de la escuela</label>
                                        <input type="text" class="form-control @error('Escuela') is-invalid  @enderror" id="Escuela" name="Escuela" placeholder="Nombre de la Escuela">
                                        <span class="text-danger">
                                            @error('Escuela')
                                            El campo es obligatorio por favor llena el nombre de la escuela
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="Iniciales">Iniciales</label>
                                        <input type="text" class="form-control @error('Iniciales') is-invalid  @enderror" id="Iniciales" name="Iniciales" placeholder="Iniciales de la escuela">
                                        <span class="text-danger">
                                            @error('Iniciales')
                                            El campo es obligatorio por favor llena las iniciales.
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="exampleInputFile">Logo de la escuela</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input  @error('Logo') is-invalid  @enderror" id="Logo" name="Logo">
                                                <label class="custom-file-label" for="exampleInputFile">Seleccionar Fotografia</label>
                                                <span class="text-danger">
                                                    @error('Logo')
                                                    El campo es obligatorio por favor agrega el logo de la escuela
                                                    @enderror
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

