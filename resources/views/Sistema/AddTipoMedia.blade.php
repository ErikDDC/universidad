@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
    @error('TMedia')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error!</strong> Por favor llena los campos en rojo correctamente
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @php
        $Registro = 0;
    @endphp
    @enderror
    @if($Registro == 1)
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Exito!</strong> Se agrego el Tipo de media Correctamente
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row justify-content-center py-auto px-auto">
            <div class="col-md-12">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-header">
                        Añadir Tipo de Media
                    </div>
                    <div class="card-body">
                        <form role="form" action="{{url('/NewTipoMedia')}}">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="TMedia">Tipo de Media</label>
                                        <input type="text" class="form-control @error('TMedia') is-invalid @enderror" id="TMedia" name="TMedia" placeholder="Tipo de Media">
                                        <span class="text-danger">
                                            @error('TMedia')
                                            El campo es obligatorio por favor llena tipo de media
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

