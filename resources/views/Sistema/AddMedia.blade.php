@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
@stop

@section('content')
    @error('Escuela')
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Error!</strong> Por favor llena los campos en rojo correctamente
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @php
        $Registro = 0;
    @endphp
    @enderror
    @if($Registro == 1)
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <strong>Exito!</strong> Se agrego el Tipo de media Correctamente
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-header">
                        Añadir Multimedia
                    </div>
                    <div class="card-body">
                        <form role="form" action="{{url('/AddMedia')}}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Escuela</label>
                                            <select class="form-control select2 @error('Escuela') is-invalid @enderror" style="width: 100%;" name="Escuela">
                                                <option selected="selected" disabled>Selecciona...</option>
                                                @foreach($universidades as $universidad)universidades
                                                <option value="{{$universidad->id}}">{{$universidad->Nombre}}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger">
                                                @error('Escuela')
                                                El campo es obligatorio
                                                @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Tipo de Media</label>
                                            <select class="form-control select2 @error('TMedia') is-invalid @enderror" style="width: 100%;" name="TMedia">
                                                <option selected="selected" disabled>Selecciona...</option>
                                                @foreach($TMedias as $TMedia)
                                                    <option value="{{$TMedia->id}}">{{$TMedia->TipoMedia}}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-danger">
                                                @error('TMedia')
                                                El campo es obligatorio
                                                @enderror
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                </div>
                                <div class="form-group">
                                    <label for="Titulo">Titulo</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="text" class="form-control @error('Titulo') is-invalid @enderror" id="Titulo" name="Titulo" placeholder="Titulo">
                                        </div>
                                    </div>
                                    <span class="text-danger">
                                                @error('Titulo')
                                                El campo es obligatorio
                                                @enderror
                                            </span>
                                </div>
                                <div class="form-group">
                                    <label>Descripción</label>
                                    <textarea class="form-control @error('Descripcion') is-invalid @enderror" rows="3" placeholder="Descripción" name="Descripcion"></textarea>
                                    <span class="text-danger">
                                                @error('Descripcion')
                                                El campo es obligatorio
                                                @enderror
                                            </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Imagen de encavezado</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input @error('IMGEnc') is-invalid @enderror" name="IMGEnc" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Seleccionar Fotografia</label>
                                        </div>
                                    </div>
                                    <span class="text-danger">
                                                @error('IMGEnc')
                                                El campo es obligatorio
                                                @enderror
                                            </span>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Media</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input @error('Media') is-invalid @enderror" name="Media" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Seleccionar Archivo Multimedia</label>
                                        </div>
                                    </div>
                                    <span class="text-danger">
                                                @error('Media')
                                                El campo es obligatorio
                                                @enderror
                                            </span>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

