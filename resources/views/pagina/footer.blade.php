<footer class="container-fluid bg-dark text-light py-4">
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <h1><a href="https://www.facebook.com/"><i class="fab fa-facebook px-2 text-primary"></i></a></h1> EPO. 358
                    </div>
                    <div class="col-md-4 text-center">
                        <h1><a class="" href="https://twitter.com/?lang=es"><i class="fab fa-twitter px-2 text-primary"></i></a></h1> @EPO. 358
                    </div>
                    <div class="col-md-4 text-center">
                        <h1><a class="" href="https://www.instagram.com/accounts/login/?hl=es-la"><i class="fab fa-instagram px-2 text-danger"></i></a></h1> EPO. 358
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
