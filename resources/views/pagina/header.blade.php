    <nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
        <a class="navbar-brand" href="{{url('/')}}"><i class="fas fa-laptop-house"></i> &nbsp; UNIVERSIDADES</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ url()->current() == url('/')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/')}}">Inicio</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/UAEM')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/UAEM')}}">UAEMEX</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/ITT')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/ITT')}}">ITT</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/UNAM')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/UNAM')}}">UNAM</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/IPN')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/IPN')}}">IPN</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/UAM')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/UAM')}}">UAM</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/UTVT')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/UTVT')}}">UTVT</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/UPVT')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/UPVT')}}">UPVT</a>
                </li>
                <li class="nav-item {{ url()->current() == url('/NORMAL')? 'active': ''}}">
                    <a class="nav-link" href="{{url('/NORMAL')}}">ESCUELAS NORMALES</a>
                </li>
            </ul>
            <div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown my-2 my-lg-0 dropleft">
                        <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Iniciar Sesión
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="" data-toggle="modal" data-target="#login"><i class="fas fa-user"></i> &nbsp;&nbsp; Inicia Sesión</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="" data-toggle="modal" data-target="#register"><i class="fas fa-user-edit"></i> &nbsp;&nbsp; Registrate</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    @include('pagina.login')
    @include('pagina.register')
