@extends('escuelas.plantilla')
@section('Contenido')
    <div class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @php
                    $cont = 0;
                @endphp
                @foreach($imagenes as $imagene)
                    <li data-target="#carouselExampleIndicators" data-slide-to="$cont" @if($cont == 0) class="Active" @endif></li>
                    {{$cont++}}
                @endforeach
            </ol>
            <div class="carousel-inner">
                @php
                    $cont1 = 0;
                @endphp
                @foreach($imagenes as $imagen)
                    @if($cont1 == 0)
                        <div class="carousel-item active">
                    @else
                        <div class="carousel-item">
                    @endif
                        <img src="{{Storage::url($imagen->Media)}}" class="d-block w-100 @if($cont1 == 0) Active @endif" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5><b>{{$imagen->Iniciales}}</b></h5>
                            <p>{{$imagen->Nombre}}</p>
                        </div>
                    </div>
                    {{$cont1++}}
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!--<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @php
                    $cont = 0;
                @endphp
                @foreach($imagenes as $imagene)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$cont}}" @if($cont == 0) class="Active" @endif></li>
                    {{$cont++}}
                @endforeach
            </ol>
            <div class="carousel-inner">
                @php
                    $cont1 = 0;
                @endphp
                @foreach($imagenes as $imagen)
                    <div class="carousel-item active">
                        <img src="{{Storage::url($imagen->Media)}}" class="d-block w-100 @if($cont1 == 0) Active @endif" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5><b>{{$imagen->Iniciales}}</b></h5>
                            <p>{{$imagen->Nombre}}</p>
                        </div>
                    </div>
                    {{$cont1++}}
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
        </div>-->
    </div>
    </div>

@endsection

