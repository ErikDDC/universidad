@extends('escuelas.plantilla')
@section('Contenido')
    <div class="container">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @php
                    $cont = 0;
                @endphp
                @foreach($imagenes as $imagene)
                    <li data-target="#carouselExampleIndicators" data-slide-to="$cont" @if($cont == 0) class="Active" @endif></li>
                    {{$cont++}}
                @endforeach
            </ol>
            <div class="carousel-inner">
                @php
                    $cont1 = 0;
                @endphp
                @foreach($imagenes as $imagen)
                    @if($cont1 == 0)
                        <div class="carousel-item active">
                            @else
                                <div class="carousel-item">
                                    @endif
                                    <img src="{{Storage::url($imagen->Media)}}" class="d-block w-100 @if($cont1 == 0) Active @endif" alt="...">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5><b>{{$imagen->Iniciales}}</b></h5>
                                        <p>{{$imagen->Nombre}}</p>
                                    </div>
                                </div>
                                {{$cont1++}}
                                @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
            </div>
        <!--<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @php
            $cont = 0;
        @endphp
        @foreach($imagenes as $imagene)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$cont}}" @if($cont == 0) class="Active" @endif></li>
                    {{$cont++}}
        @endforeach
            </ol>
            <div class="carousel-inner">
                @php
            $cont1 = 0;
        @endphp
        @foreach($imagenes as $imagen)
            <div class="carousel-item active">
                <img src="{{Storage::url($imagen->Media)}}" class="d-block w-100 @if($cont1 == 0) Active @endif" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5><b>{{$imagen->Iniciales}}</b></h5>
                            <p>{{$imagen->Nombre}}</p>
                        </div>
                    </div>
                    {{$cont1++}}
        @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
        </div>-->
        </div>
        <div class="container my-4">
            <div class="row">
                <div class="col-12 my-2 text-center">
                    <h1><b class="text-primary">Etapa 1.</b></h1>
                </div>
            </div>
            <div class="card-deck">
                @foreach($videos as $video)
                    <div class="col-md-4">
                        <div class="card">
                            <img src="{{Storage::url($video->Encabezado)}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$video->Titulo}}</h5>
                                <p class="card-text">{{$video->Descripcion}}</p>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#video{{$video->id}}">Abrir</button>
                            </div>
                        </div>
                    </div>
                    <!----------------------------Videos--------------------------->
                    <div class="modal fade" id="video{{$video->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="{{Storage::url($video->Media)}}?rel=0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container my-4">
            <div class="row">
                <div class="col-12 my-2 text-center">
                    <h1><b class="text-primary">Etapa 2.</b></h1>
                </div>
            </div>
            <div class="card-deck">
                @foreach($infografias as $infografia)
                    <div class="col-md-4">
                        <div class="card">
                            <img src="{{Storage::url($infografia->Encabezado)}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$infografia->Titulo}}</h5>
                                <p class="card-text">{{$infografia->Descripcion}}</p>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#video{{$infografia->id}}">Abrir</button>
                            </div>
                        </div>
                    </div>
                    <!----------------------------Videos--------------------------->
                    <div class="modal fade" id="video{{$infografia->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="{{Storage::url($infografia->Media)}}?rel=0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container my-4">
            <div class="row">
                <div class="col-12 my-2 text-center">
                    <h1><b class="text-primary">Etapa 3.</b></h1>
                </div>
            </div>
            <div class="card-deck">
                @foreach($etapa3 as $infografia)
                    <div class="col-md-4">
                        <div class="card">
                            <img src="{{Storage::url($infografia->Encabezado)}}" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">{{$infografia->Titulo}}</h5>
                                <p class="card-text">{{$infografia->Descripcion}}</p>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-warning" data-toggle="modal" data-target="#video{{$infografia->id}}">Abrir</button>
                            </div>
                        </div>
                    </div>
                    <!----------------------------Videos--------------------------->
                    <div class="modal fade" id="video{{$infografia->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-xl modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="{{Storage::url($infografia->Media)}}?rel=0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

@endsection

