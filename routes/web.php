<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('escuelas.inicio');
});*/

Auth::routes();

Route::get('/', 'Sistema\Pagina\PaginaController@Inicio')->name('Inicio');
Route::get('/ITT', 'Sistema\Pagina\PaginaController@ITT')->name('ITT');
Route::get('/UMB', 'Sistema\Pagina\PaginaController@UMB')->name('UMB');
Route::get('/TEST', 'Sistema\Pagina\PaginaController@TEST')->name('TEST');
Route::get('/UAEM', 'Sistema\Pagina\PaginaController@UAEM')->name('UAEM');
Route::get('/IPN', 'Sistema\Pagina\PaginaController@IPN')->name('IPN');
Route::get('/UPVT', 'Sistema\Pagina\PaginaController@UPVT')->name('UPVT');
Route::get('/UTVT', 'Sistema\Pagina\PaginaController@UTVT')->name('UTVT');
Route::get('/UNAM', 'Sistema\Pagina\PaginaController@UNAM')->name('UNAM');
Route::get('/UAM', 'Sistema\Pagina\PaginaController@UAM')->name('UAM');
Route::get('/NORMAL', 'Sistema\Pagina\PaginaController@NORMAL')->name('NORMAL');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/AddMedia', 'Sistema\Formularios\FormController@ViewFormMedia')->name('AddMedia');
Route::post('/AddMedia', 'Sistema\Formularios\FormController@NewMedia')->name('NewMedia');

Route::get('/AddUniversidad', 'Sistema\Catalogos\CatalogosController@ViewAddUniversidad')->name('AddUniversidad');
Route::post('/NewUniversidad', 'Sistema\Catalogos\CatalogosController@NewUniversidad')->name('NewUniversidad');

Route::get('/AddTipoMedia', 'Sistema\Catalogos\CatalogosController@ViewAddTipoMedia')->name('AddTipoMedia');
Route::get('/NewTipoMedia', 'Sistema\Catalogos\CatalogosController@NewTipoMedia')->name('NewTipoMedia');

