<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipomedia extends Model
{
    protected $table = 'tipomedia';
    protected $fillable = [
        'id',
        'TipoMedia',
        'Estatus',
    ];
}
