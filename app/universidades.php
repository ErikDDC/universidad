<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class universidades extends Model
{
    protected $table = 'universidades';
    protected $fillable = [
        'id',
        'Nombre',
        'Iniciales',
        'Logo',
        'Estatus',
    ];
}
