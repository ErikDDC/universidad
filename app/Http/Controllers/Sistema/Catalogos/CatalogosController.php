<?php

namespace App\Http\Controllers\Sistema\Catalogos;

use App\Http\Controllers\Controller;
use App\tipomedia;
use App\universidades;
use Illuminate\Http\Request;

class CatalogosController extends Controller
{
    public function ViewAddUniversidad(){
        $Registro = 0;
        return view('Sistema.AddEscuelas', ['Registro'=>$Registro]);

    }
    public function NewUniversidad(Request $request){
        $request->validate([
            'Escuela' => 'required',
            'Iniciales' => 'required',
            'Logo' => 'required',
        ]);

        $Inserccion = universidades::create([
            'Nombre'=>$request->input('Escuela'),
            'Iniciales'=>$request->input('Iniciales'),
            'Logo'=> $request->file('Logo')->store('public/Imagenes/Universidades'),
            'Estatus'=>'1',
        ]);
        $Registro = 1;
        return view('Sistema.AddEscuelas', ['Registro'=>$Registro]);
    }

    // Catalogo Tipo Media
    public function ViewAddTipoMedia(){
        $Registro = 0;
        return view('Sistema.AddTipoMedia', ['Registro'=>$Registro]);
    }

    public function NewTipoMedia(Request $request){
        $request->validate([
            'TMedia' => 'required',
        ]);
        $Inserccion = tipomedia::create([
            'TipoMedia'=>$request->input('TMedia'),
            'Estatus'=>'1',
        ]);
        $Registro = 1;
        return view('Sistema.AddTipoMedia', ['Registro'=>$Registro]);
    }
}
