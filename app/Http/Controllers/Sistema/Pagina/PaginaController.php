<?php

namespace App\Http\Controllers\Sistema\Pagina;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaginaController extends Controller
{
    public function Inicio(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')->get();

        return view('escuelas.inicio', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function ITT(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','1')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','1')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','1')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','1')->get();

        return view('escuelas.ITT', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UMB(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','2')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','2')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','2')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','2')->get();

        return view('escuelas.UMB', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function TEST(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','3')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','3')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','3')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','3')->get();

        return view('escuelas.TEST', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UAEM(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','2')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','2')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','2')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','2')->get();

        return view('escuelas.UAEM', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UNAM(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','3')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','3')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','3')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','3')->get();

        return view('escuelas.UNAM', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function IPN(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','4')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','4')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','4')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','4')->get();

        return view('escuelas.IPN', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UAM(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','5')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','5')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','5')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','5')->get();

        return view('escuelas.UAM', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UPVT(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','7')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','7')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','7')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','7')->get();

        return view('escuelas.IPVT', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function NORMAL(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','8')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','8')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','8')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','8')->get();

        return view('escuelas.NORMAL', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UTVT(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','6')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','6')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','6')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','6')->get();

        return view('escuelas.UTVT', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
    public function UNITEC(){

        $videos = DB::table('multimedia')->where('tipomedia_id','=','1')->where('universidad_id','=','6')->get();
        $infografias = DB::table('multimedia')->where('tipomedia_id','=','2')->where('universidad_id','=','6')->get();
        $etapa3 = DB::table('multimedia')->where('tipomedia_id','=','4')->where('universidad_id','=','6')->get();
        $imagenes = DB::table('multimedia')
            ->join('universidades','universidades.id','=','multimedia.universidad_id')
            ->where('tipomedia_id','=','3')
            ->where('universidad_id','=','6')->get();

        return view('escuelas.UNITEC', [
            'videos' => $videos,
            'infografias' => $infografias,
            'imagenes' => $imagenes,
            'etapa3' => $etapa3
        ]);
    }
}
