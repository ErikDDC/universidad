<?php

namespace App\Http\Controllers\Sistema\Formularios;

use App\Http\Controllers\Controller;
use App\multimedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    public function ViewFormMedia(){

        $universidades = DB::table('universidades')->where('Estatus','=','1')->get();
        $TMedias = DB::table('tipomedia')->where('Estatus','=','1')->get();

        $Registro = 0;

        return view('Sistema.AddMedia', ['universidades' => $universidades, 'TMedias' => $TMedias, 'Registro'=>$Registro]);
    }

    public function NewMedia(Request $request){
        $request->validate([
            'Escuela' => 'required',
            'TMedia' => 'required',
            'Titulo' => 'required',
            'Descripcion' => 'required',
            'IMGEnc' => 'required',
            'Media' => 'required',
        ]);

        $Inserccion = multimedia::create([
            'universidad_id' => $request->input('Escuela'),
            'tipomedia_id' => $request->input('TMedia'),
            'Titulo' => $request->input('Titulo'),
            'Descripcion' => $request->input('Descripcion'),
            'Encabezado' => $request->file('IMGEnc')->store('public/Imagenes/Universidades'),
            'Media' => $request->file('Media')->store('public/Media/Universidades'),
            'Estatus'=>'1',
        ]);
        $Registro = 1;
        $universidades = DB::table('universidades')->where('Estatus','=','1')->get();
        $TMedias = DB::table('tipomedia')->where('Estatus','=','1')->get();
        return view('Sistema.AddMedia', ['universidades' => $universidades, 'TMedias' => $TMedias, 'Registro'=>$Registro]);
    }
}
