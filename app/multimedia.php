<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class multimedia extends Model
{
    protected $table = 'multimedia';
    protected $fillable = [
        'id',
        'universidad_id',
        'tipomedia_id',
        'Titulo',
        'Descripcion',
        'Encabezado',
        'Media',
        'Estatus',
    ];
}
