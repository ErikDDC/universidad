<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultimediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multimedia', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('universidad_id');
            $table->foreign('universidad_id')->references('id')->on('universidades');
            $table->unsignedBigInteger('tipomedia_id');
            $table->foreign('tipomedia_id')->references('id')->on('tipomedia');
            $table->string('Titulo');
            $table->longText('Descripcion');
            $table->longText('Encabezado');
            $table->longText('Media');
            $table->boolean('Estatus')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_multimedia');
    }
}
