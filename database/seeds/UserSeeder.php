<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Erik Daniel Diaz Campuzano',
            'email' => 'danielerik52@hotmail.com',
            'password' => Hash::make('secret'),
        ]);
    }
}
