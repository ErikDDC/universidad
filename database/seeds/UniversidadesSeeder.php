<?php

use Illuminate\Database\Seeder;

class UniversidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('universidades')->insert([
            'Nombre' => 'Centro de Información ITT',
            'Iniciales' => 'ITT',
            'Logo' => 'SD',
        ]);
        DB::table('universidades')->insert([
            'Nombre' => 'Universidad Autónoma del Estado de México',
            'Iniciales' => 'UAEM',
            'Logo' => 'SD',
        ]);
        DB::table('universidades')->insert([
            'Nombre' => 'Universidad Nacional Autónoma de México',
            'Iniciales' => 'UNAM',
            'Logo' => 'SD',
        ]);
        DB::table('universidades')->insert([
            'Nombre' => 'Instituto Politécnico Nacional',
            'Iniciales' => 'IPN',
            'Logo' => 'SD',
        ]);
        DB::table('universidades')->insert([
            'Nombre' => ' Universidad Politécnica del Valle de Toluca',
            'Iniciales' => 'UPVT',
            'Logo' => 'SD',
        ]);
        DB::table('universidades')->insert([
            'Nombre' => 'Universidad Tecnológica de México',
            'Iniciales' => 'UNITEC',
            'Logo' => 'SD',
        ]);
    }
}
