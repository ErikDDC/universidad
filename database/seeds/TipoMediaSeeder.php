<?php

use Illuminate\Database\Seeder;

class TipoMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipomedia')->insert([
            'TipoMedia' => 'Infografia',
        ]);
        DB::table('tipomedia')->insert([
            'TipoMedia' => 'Video',
        ]);
        DB::table('tipomedia')->insert([
            'TipoMedia' => 'Fotografia',
        ]);
    }
}
